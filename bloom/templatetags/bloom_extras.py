from django import template

register = template.Library()


@register.filter(name='get_nice_name')
def get_nice_name(value):
    return value[0:value.index('(')]
