from django.contrib import admin
from bloom.models import Taxonomy,Word,Guest

# Register your models here.

admin.site.register(Taxonomy)
admin.site.register(Word)
admin.site.register(Guest)