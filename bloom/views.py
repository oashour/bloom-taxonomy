﻿from __future__ import unicode_literals
from django.shortcuts import render,redirect
from django.http import HttpResponse,HttpResponseRedirect,Http404
from bloom.models import Taxonomy,Word,Guest,UserAccount,UserSavedTree
from django.core import serializers
import json
import datetime
from django.core.exceptions import ObjectDoesNotExist
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.core.servers.basehttp import FileWrapper
from bloom.forms import *
import hashlib

def test(request):
    if request.method == 'POST': # If the form has been submitted...
        form = UploadFileForm(request.POST,request.FILES) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass            
            file = request.FILES['file']
            data = u'' 
            for chunk in file.chunks(): 
                try:                
                    data += chunk.decode('ascii','ignore') 
                except UnicodeDecodeError:
                    break
                
                

            #if data:
                #data = data.decode('ascii', 'xmlcharrefreplace')
				
            form = UploadFileForm()   
			 
            #if  data and data.strip().startswith('<div class="tree"') and data.strip().endswith('</div>') :
            if  data and data.strip().startswith('<div') and data.strip().endswith('</div>') :    
                return render(request,'bloom/upload.html',{'data':data.strip()}) # Redirect after POST
                        
            return render(request, 'bloom/upload.html', { 'form': form, })
    else:
        form = UploadFileForm() # An unbound form

        return render(request, 'bloom/upload.html', {
            'form': form,
        })
    
def help(request):
    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')

    

    return render(request,"bloom/help.html")

    

def contact(request):
    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')

    

    return render(request,"bloom/contact.html")


def about(request):
    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')

    

    return render(request,"bloom/about.html")


def index(request,action='new'):
    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')

    request.session['tree'] = ''
    rdata = {}
    rdata['user_name'] = request.session.get('user_name','') 
    rdata['user_id'] = request.session.get('user_id',0)
    rdata['Taxonomy'] = Taxonomy.objects.all()
    rdata['Words'] = Word.objects.select_related('Taxonomy').all()

    return render(request,"bloom/index.html",{'rdata':rdata})

def generator(request):    

    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')
    
    if request.is_ajax():
        json_tree = request.POST.get('json_tree','')        
        if(json_tree):
            request.session['json_tree'] = json_tree

        return HttpResponse(json_tree)
    
    request.session['tree'] = ''
    rdata = {}
    rdata['json_tree'] = request.session.get('json_tree','') 
    rdata['user_name'] = request.session.get('user_name','') 
    rdata['Taxonomy'] = Taxonomy.objects.all()

    return render(request,"bloom/generator/generator.html",{'rdata':rdata})


def generate_sentence(request,action='new'):
    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')

    request.session['tree'] = ''
    rdata = {}
    rdata['user_name'] = request.session.get('user_name','') 
    rdata['Taxonomy'] = Taxonomy.objects.all()

    return render(request,"bloom/generator/generate_sentence.html",{'rdata':rdata})

def save_tree1(request):
    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')

    if request.is_ajax():
        
        tree = request.POST.get('tree','')
        request.session['tree'] = tree;
            
        return HttpResponse('done')
    else:
        raise Http404

def download_tree(request):
    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')    
        
    tree = request.session.get('tree','')
    #tree = '<ul><li><img parent_node="1" node_counter="1" taxonomy_id="1" src="/static/bloom/images/Pencil-icon.png" style="border: 0px none; display: inline;" alt="eidt" class="edit" id="edit_wrd_idundefined"><a parent_node="0" node_counter="1" taxonomy_id="1" class="tree_node" href="#">أن يتعرف الطالب sss</a><ul><li><img parent_node="1" node_counter="1" taxonomy_id="1" src="/static/bloom/images/Pencil-icon.png" style="border: 0px none; display: inline;" alt="eidt" class="edit" id="edit_wrd_idundefined"><a parent_node="1" node_counter="1" taxonomy_id="1" class="tree_node selected" href="#">aa</a><ul><li><img parent_node="1" node_counter="1" taxonomy_id="1" src="/static/bloom/images/Pencil-icon.png" style="border: 0px none; display: inline;" alt="eidt" class="edit" id="edit_wrd_idundefined"><a parent_node="1" node_counter="2" taxonomy_id="1" class="tree_node" href="#">bb</a></li><li><img parent_node="1" node_counter="1" taxonomy_id="1" src="/static/bloom/images/Pencil-icon.png" style="border: 0px none; display: inline;" alt="eidt" class="edit" id="edit_wrd_idundefined"><a parent_node="1" node_counter="2" taxonomy_id="1" class="tree_node" href="#">ccc</a></li><li><img parent_node="1" node_counter="1" taxonomy_id="1" src="/static/bloom/images/Pencil-icon.png" style="border: 0px none; display: inline;" alt="eidt" class="edit" id="edit_wrd_idundefined"><a parent_node="1" node_counter="1" taxonomy_id="1" class="tree_node new" href="#">dd</a></li></ul></li></ul></li></ul>'
    #tree = u' '.join(tree).encode('utf-8').strip()    
    if tree :
        tree = tree.encode('ascii', 'xmlcharrefreplace')
        temp = NamedTemporaryFile()
        temp.write(tree)
        #temp.flush()
        #img_temp.close()
        #temp.seek(0)
        #im.file.save("test.txt", File(img_temp))
        #user_id = request.session.get('user_id',0) 
        #tree = UserSavedTree.objects.get(UserAccount_id__exact=user_id)  
        #return HttpResponse('Error'+str(temp.read()))
        wrapper = FileWrapper(temp)
        response = HttpResponse(wrapper, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=data.txt'
        response['Content-Length'] = temp.tell()
        temp.seek(0)

        return response
        
    

def save_tree(request):
    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')

    if request.is_ajax():
        try:
            tree = request.POST.get('tree','')
            user_id = request.session.get('user_id',0) 
            user_saved_tree = UserSavedTree()

            if tree and user_id :
                try:
                    user_saved_tree = UserSavedTree.objects.get(UserAccount_id__exact=user_id)   
                    user_saved_tree.Date = datetime.datetime.now()
                    user_saved_tree.Tree = tree

                    user_saved_tree.save()                         
                    #wrd = taxonomy.word_set.create(Word=verb);                       
                    #response_data = {}
                    #response_data['word'] = serializers.serialize('json',wrd)
                    #return HttpResponse(json.dumps(response_data),content_type="application/json")                    
                    return HttpResponse(user_saved_tree)                    
                                        
                except ObjectDoesNotExist:
                    date = datetime.datetime.now()
                    user_saved_tree = UserSavedTree.objects.create(UserAccount_id=user_id,Tree=tree,Date=date)
                    user_saved_tree.save()    

        except KeyError:
            return HttpResponse('Error') # incorrect post
        # do stuff, e.g. calculate a score
        return HttpResponse('NO')
    else:
        raise Http404

def open_tree(request):
    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')

    if request.is_ajax():
        try:            
            user_id = request.session.get('user_id',0)            
            if user_id :
                try:
                    user_saved_tree = UserSavedTree.objects.get(UserAccount_id__exact=user_id)   
                    return HttpResponse(user_saved_tree)                    
                                        
                except ObjectDoesNotExist:
                  return HttpResponse('Null') # incorrect post

        except KeyError:
            return HttpResponse('Error') # incorrect post
        # do stuff, e.g. calculate a score
        return HttpResponse('Null')
    else:
        raise Http404

def logout(request):
    request.session.flush()
    return redirect('/bloom/login/')   
    


def login(request):
    msg = ''
    if request.POST.get('submit',''):
        if not (request.POST.get('email',0) and request.POST.get('password',0)) :
            msg = 'يجب ملئ جميع البيانات'
        else:
            email = request.POST.get('email','')
            password = request.POST.get('password','')     
            try:
            
                user_account = UserAccount.objects.get(Email=email,Password=password)                  
                request.session['is_logon'] = 1

                request.session['user_name'] = user_account.Name
                request.session['user_id'] = user_account.id

                return redirect('/bloom/index/new')

            except ObjectDoesNotExist:
                msg = 'الرجاء التأكد من صحة اسم المستخدم او كلمة المرور '
            
             
                
    return render(request,"bloom/login.html",{'msg':msg})     

def register(request):
    msg = ''
    if request.POST.get('submit',''):
        if not (request.POST.get('name',0) and request.POST.get('email',0) and request.POST.get('password',0) and request.POST.get('cpassword',0)) :
            msg = 'يجب ملئ جميع البيانات'

        elif ( request.POST.get('password') !=  request.POST.get('cpassword') ):
            msg = 'كلمتا المرور غير متطابقتين'

        else:
            
            name =  request.POST.get('name','')  
            email = request.POST.get('email','')
            password = request.POST.get('password','')            
            date = datetime.datetime.now()


            cnt = UserAccount.objects.filter(Email=email).count();  

            if cnt > 0 :
                msg = 'البريد الالكتروني المدخل موجود مسبقا، الرجاء تحديد بريد اكتروني اخر'

            else:
                user_account = UserAccount(Name=name,Email=email,Password=password,Date=date,tree='')
                user_account.save()

                request.session['is_logon'] = 1
                request.session['user_name'] = name

                return redirect('/bloom/index/new')
        

    return render(request,"bloom/register.html",{'msg':msg})

def get_words(request):
    x = []

    for w in Word.objects.order_by('Taxonomy').all():
        xx = {'value':w.id,'label':w.Word}
        x.append(xx)

    #json_data = serializers.serialize('json',  x)
    #json_data = serializers.serialize('json', Word.objects.all(), fields=('id','Word'))
    json_data = json.dumps(x,ensure_ascii=False).encode('utf8')
    return HttpResponse(json_data, mimetype='application/json;charset=utf-8')
    #return HttpResponse(x)

def verb_add(request):
    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')

    if request.is_ajax():
        try:
            taxonomy_id = int(request.POST.get('taxonomy_id',0))
            verb = str(request.POST.get('verb',0))
                        
            if taxonomy_id and verb :
                try:
                    taxonomy = Taxonomy.objects.get(id=taxonomy_id)
                    wrd = taxonomy.word_set.create(Word=verb)            
                    x = {'id':wrd.id, 'taxonomy': taxonomy.Name}
                    json_data = json.dumps(x,ensure_ascii=False).encode('utf8')

                    return HttpResponse(json_data, content_type="application/json")                    
                    #return HttpResponse(response_data)                    
                                        
                except ObjectDoesNotExist:
                    return HttpResponse('Error') # incorrect post

        except KeyError:
            return HttpResponse('Error') # incorrect post
        # do stuff, e.g. calculate a score
        return HttpResponse(str(score))
    else:
        raise Http404

def verb_update(request):
    if not request.session.get('is_logon',0):
        return redirect('/bloom/login')
    if request.is_ajax():
        try:
            wrd_id = int(request.POST.get('wrd_id',0))
            verb = str(request.POST.get('verb',0))
                        
            if wrd_id and verb :
                try:                                                          
                    wrd = Word.objects.get(pk=wrd_id)
                    wrd.Word = verb
                    wrd.save();
                    #response_data['word'] = serializers.serialize('json',wrd)
                    #return HttpResponse(json.dumps(response_data),content_type="application/json")                    
                    return HttpResponse(str(wrd.Word))    
                
                except ObjectDoesNotExist:
                    return HttpResponse('Error') # incorrect post

        except KeyError:
            return HttpResponse('Error') # incorrect post
        # do stuff, e.g. calculate a score
        return HttpResponse(str(score))
    else:
        raise Http404

def pdf(request,action='show'):
    #if not request.session.get('is_logon',0):
    #    return redirect('/bloom/login')
    user_id = request.GET.get('id',0)
    if user_id == 0:
        return HttpResponseBadRequest();

    try:
        user_account = UserAccount.objects.get(pk=user_id)

        if action == 'show':
            tree = user_account.tree;       
            return render(request,"bloom/pdf.html",{'tree':tree})

        elif action == 'save':
            if request.is_ajax():
                try:            
                    tree = request.POST.get('tree',0)            
                    #request.session['tree'] = tree

                    user_account.tree = tree
                    user_account.save(force_update=True, update_fields=['tree'])

                    return HttpResponse(tree)
                except KeyError:
                    return HttpResponse('Error') # incorrect post
                # do stuff, e.g. calculate a score
            
            else:
                raise Http404

        else:
              return HttpResponse('Error') # incorrect post

    except ObjectDoesNotExist:
       return HttpResponse('Error') # incorrect post

    