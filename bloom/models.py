from django.db import models

# Create your models here.
class Taxonomy (models.Model):
    Name = models.CharField(max_length=100)
    Description = models.CharField(max_length=500)

    def __str__(self):
        return self.Name

class Word(models.Model):
    Taxonomy = models.ForeignKey(Taxonomy)
    Word = models.CharField(max_length=300)

    def __str__(self):
        return self.Word

class RelatedWord(models.Model):
    Word = models.ForeignKey(Word)
    RelatedWord = models.CharField(max_length=300)

    def __str__(self):
        return self.RelatedWord

class Guest(models.Model):
    Name = models.CharField(max_length=100)
    Email = models.EmailField(max_length=75)
    Age = models.DecimalField(max_digits=3,decimal_places=0)
    Country = models.CharField(max_length=100)
    Date    = models.DateTimeField(auto_now=1, auto_now_add=1)

    def __str__(self):
        return self.Name

class UserAccount(models.Model):
    Name = models.CharField(max_length=100)
    Email = models.EmailField(max_length=75)
    Password = models.CharField(max_length=20)
    Date    = models.DateTimeField(auto_now=1, auto_now_add=1)
    tree = models.TextField(null=1,default='')


    def __str__(self):
        return self.Name


class UserWords(models.Model):
    UserAccount = models.ForeignKey(UserAccount)
    Word = models.ForeignKey(Word)

    def __str__(self):
        return self.UserAccount.Name,' ',self.Word.Word


class UserSavedTree(models.Model):
    UserAccount = models.ForeignKey(UserAccount)
    Tree = models.TextField()
    Date = models.DateTimeField(auto_now=1, auto_now_add=1)

    def __str__(self):
        return self.Tree