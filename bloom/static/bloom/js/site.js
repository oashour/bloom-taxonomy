function postGenerate(event) {
    generateTree();
    json_tree = JSON.stringify(BloomTree);
    //alert(tree);
    $.post("/bloom/generator/", {
        csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
        'json_tree': json_tree

    }).done(function(data) {
        //window.location = "/bloom/generator/";
        window.open('/bloom/generator/', '_blank');
    })

    event.preventDefault();

} // end function postGenerate


function genertateStatistics(event) {
    var $tree = $("#tree");
    var statistics = {
        taxs: [],
        total: 0
    };
    $.each($tree.find('a'), function(index, val) {
        var $val = $(val);
        var taxid = $val.attr('data-tax-id');

        if (!statistics.taxs.hasOwnProperty(taxid)) {
            statistics.taxs[taxid] = 1;
        } else {
            statistics.taxs[taxid] = statistics.taxs[taxid] + 1;
        }


        statistics.total = statistics.total + 1;
    }); //end each

    console.dir(statistics);

    $('#dialog-statistics thead tr:gt(0) td').empty();
    if (statistics.taxs.length > 0) {
        for (var i = 1; i < 7; i++) {

            var value = 0;
            if (statistics.taxs.hasOwnProperty(i)) {
                value = statistics.taxs[i];
            }
            var percent = (value / statistics.total) * 100;

            $('#dialog-statistics tbody tr:eq(0) td').eq(i - 1).text(value);
            $('#dialog-statistics tbody tr:eq(1) td').eq(i - 1).text(percent.toFixed(2) + ' %');
            $('#dialog-statistics tbody tr:eq(2) td:eq(0)').text(statistics.total);
        }

        $("#dialog-statistics").dialog('open');
    }else{
    	alert('لا يوجد بيانات');
    }


    event.preventDefault();
} //end function statistics


function genertateStatistics1() {
    var $tree = $("#tree");
    var statistics = {
        taxs: [],
        total: 0
    };
    $.each($tree.find('a'), function(index, val) {
        var $val = $(val);
        var taxid = $val.attr('data-tax-id');

        if (!statistics.taxs.hasOwnProperty(taxid)) {
            statistics.taxs[taxid] = 1;
        } else {
            statistics.taxs[taxid] = statistics.taxs[taxid] + 1;
        }


        statistics.total = statistics.total + 1;
    }); //end each


    $('.statistics tr:gt(0) td').empty();
    
    if (statistics.taxs.length > 0) {
        for (var i = 1; i < 7; i++) {

            var value = 0;
            if (statistics.taxs.hasOwnProperty(i)) {
                value = statistics.taxs[i];
            }
            var percent = (value / statistics.total) * 100;

            $('.statistics tbody tr:eq(0) td').eq(i - 1).text(value);
            $('.statistics tbody tr:eq(1) td').eq(i - 1).text(percent.toFixed(2) + ' %');
            $('.statistics tbody tr:eq(2) td:eq(0)').text(statistics.total);
        }

    }
} //end function 




function generatePdf(event, id) {
    var $tree = $("#tree").clone()
    $('img', $tree).remove();    
    //alert(tree);
    $.post("/bloom/pdf/save?id=" + id, {
        csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
        'tree': $tree.html()
    }).done(function(data) {

        window.open('/bloom/pdf/show?id=' + id, '_blank');
    })


    event.preventDefault();
} //end function generatePdf



function openSavedTree(event) {
    var $iframe = $('#dialog-upload iframe');
    $iframe.attr('src', $iframe.attr('src'));

    $("#dialog-upload").dialog('open');
    event.preventDefault();    
} //end function openSavedTree


function saveTree(event) {
    var $tree = $("#tree").clone();

    $.post("/bloom/save_tree1/", {
        csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
        'tree': $tree[0].outerHTML
    }).done(function(data) {
        //$("body").append("<iframe src='/bloom/save_tree_download/' style='display: none;' ></iframe>");
        $("#download").attr('src', '/bloom/download_tree/');
        //alert('تمت العملية بنجاح')
    })

    event.preventDefault();
} //end function saveTree


function generateNewTree(event, callback) {
    while(true){
        var project_name = $('#project-name').text();
        if(!project_name){
            project_name = prompt("الرجاء ادخال اسم المشروع", "");
        }

        if( project_name ){
            $('#tree').attr('project-name', project_name);
            $('#project-name').text(project_name);
            $('#tree').html('')            
            break;
        }//end if
    }//end while

    $('.maximize-box').hide();
    genertateStatistics1();
    if(event){
        event.preventDefault();
    }
    
    if( callback ) callback();
} //end function generateNewTree

function setProjectName(project_name, parent){
    if(!project_name){
        project_name = $('#tree', parent).attr('project-name');
    }
    $('.maximize-box', parent).hide();
    $('#project-name', parent).text( project_name );
    $('#dialog-interactive-project-name', parent).text( project_name );

}



var BloomTree = null;

function generateTree() {
    BloomTree = null;
    var $root_node = $('>ul>li>a:eq(0)', '#tree');
    if ($root_node.size() > 0) {

        BloomTree = {
            sentence: $root_node.text(),
            index: $root_node.prev('img').attr('node_counter'),
            parent_node: $root_node.attr('parent_node'),
            taxonomy_id: $root_node.attr('taxonomy_id'),
            childs: []
        };

        addNodeToTree($root_node.parent(), BloomTree)
    }

} //end generateTree function

function addNodeToTree($parent_tag, parent) {
    var $child_nodes = $('>ul>li>a', $parent_tag);

    $child_nodes.each(function() {
        var data = {
            sentence: $(this).text(),
            index: $(this).prev('img').attr('node_counter'),
            parent_node: $(this).attr('parent_node'),
            taxonomy_id: $(this).attr('taxonomy_id'),
            childs: []
        };

        $li = $(this).parent();
        if ($li.has('ul')) {
            addNodeToTree($li, data);
        } // end if 

        parent.childs.push(data);

    }); //end foreach

} // end function addNodeToTree


function maximizeDialogInteractive(){
    var $currentItem = $('[data-interactive-current]');
    if($currentItem.size()<1){
        return;
    }

    $('[data-interactive-current]').trigger('dblclick')
    $('.maximize-box').hide();

}