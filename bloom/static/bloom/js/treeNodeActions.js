var node_counter = 1;
var ar_taxs = ['التذكر', 'الفهم والاستيعاب', 'التطبيق', 'التحليل', 'التركيب', 'التقويم'];


function addTreeNode() {
    var $combobox_word = $('#combobox_word');
    var $txt_rest = $('#txt_rest');
    var parent_node = 0;


    if ($combobox_word.val() == '') {
        alert("يجب تحديد الفعل");
        return false;
    }
    if (!$txt_rest.val().trim()) {
        alert("يجب تحديد الجملة");
        return false;
    }

    if ($('#tree a').size() > 0 && $('#tree a.selected').size() < 1) {
        alert("يجب تحديد الاب");
        return false;
    }

    parent_node = $('#tree a.selected').size() > 0 ? $('#tree a.selected').attr('node_counter') : 0;

    //var wrds = $this.closest("[id^='tabs-']").attr('wrds').split(',');

    var $selected = $combobox_word.find(":selected")
    var taxonomy_id = $selected.attr('data-taxonomyid');
    var wrdid = $combobox_word.val();
    var wrd = $selected.text();
    var tax = $selected.attr('data-taxonomy');
    var tax_nice_name = $selected.attr('data-taxonomy-nice-name');
    // if (wrds.indexOf($txt_search.val().trim()) == -1) {

    //     $("#dialog-confirm").find('#verb').
    //                  text($txt_search.val().trim()).
    //                  attr('taxonomy_id', taxonomy_id);
    //     $("#dialog-confirm").dialog("open");
    //     return false;

    // }



    $('a.new').removeClass('new');

    var item = $('<a></a>').attr({
            'data-wrd-id': wrdid,
            'data-wrd': wrd,
            'data-sentence': formatPlainText(wrd, $txt_rest.val()),
            'data-rest-text': $txt_rest.val(),
            'href': '#',
            'class': 'tree_node new',
            'data-taxonomy-id': taxonomy_id,
            'data-tax-id': taxonomy_id,
            'data-tax': tax,
            'data-taxonomy-nice-name': tax_nice_name,
            'data-node-counter': node_counter,
            'data-parent-node': parent_node,
            'onclick':'selectTreeNode(event, this)',
            'ondblclick':'openInteractiveDialog(event, this)'
        })
        .html(formatText(wrd, $txt_rest.val(), ar_taxs[taxonomy_id - 1]))
        /*.click(function(){
        		$('a.selected').removeClass('selected')
        		$(this).addClass('selected');			
        		return false;			
        	})*/

    if ($('#tree a').size() < 1) {
        $('li:first', '<ul><li></li></ul>').
        append(item).parent().appendTo('#tree');
    } else {
        if ($('a.selected').next('ul').size() < 1) {
            $('a.selected').after(
                $('li:first', '<ul><li></li></ul>').append(item).parent())
        } else {
            $('a.selected').next('ul').append(
                $('<li></li>').append(item))
        }

    }
    $('#tree a').each(function(index, val) {
    	var $item = $(val);
        if ($item.prev().size() == 0) {
            var img = $editImg.clone();
            img.attr('onclick','editTreeNode(this)');
            img.show();
            $item.parent().prepend(img);
        }
    })// end each

    $txt_rest.val('');
    node_counter++;

    
    //document.cookie = 'tree='+$("#tree").clone().html();
    var $tree = $("#tree").clone();
    $('img:not(.edit)', $tree).remove();
    setCookie("tree", encodeURIComponent($tree.html()), 365);

    genertateStatistics1();

}

function cancelEditingTreeNote() {

    $('.update, .cancel, .delete').hide();
    $('.add').show();

    $("[in_editing]").removeAttr('in_editing');
    $('#txt_rest').val('');

    $('.search-box').removeClass('search-box-edit');
}


function updateTreeNode() {
	  	var $txt_rest = $('#txt_rest');
        var $combobox_word = $('#combobox_word');

        var $selected = $combobox_word.find(":selected")
        var taxonomy_id = $selected.attr('data-taxonomyid');
        var wrdid = $combobox_word.val();
        var wrd = $selected.text();
        var tax = $selected.attr('data-taxonomy');



        if ($combobox_word.val() == '') {
            alert("يجب تحديد الفعل");
            return false;
        }
        if (!$txt_rest.val().trim()) {
            alert("يجب تحديد الجملة");
            return false;
        }

        $("[in_editing] > a").attr({
                'data-wrd-id': wrdid,
                'data-wrd': wrd,
                'data-sentence': formatPlainText(wrd, $txt_rest.val()),
                'data-rest-text': $txt_rest.val(),
                'data-taxonomy-id': taxonomy_id,
                'data-tax-id': taxonomy_id,
                'data-tax': tax
            })
            .html(formatText(wrd, $txt_rest.val(), ar_taxs[taxonomy_id - 1]))

        $("[in_editing]").removeAttr('in_editing');
        $txt_rest.val('');

        $("#btn_cancel").trigger('click');

        var $tree = $("#tree").clone()
        $('img', $tree).remove();
        setCookie("tree", encodeURIComponent($tree.html()), 365);
        genertateStatistics1();
}


function formatText(word, restText, arTax) {
    return 'أن ' + word + ' الطالب ' + restText + '<span class="taxonomy-span">(' + arTax + ')</span>'
}
function formatPlainText(word, restText) {
    return 'أن ' + word + ' الطالب ' + restText
}

function editTreeNode (element) {
	 var $target = $(element).next();
    var wrd_id = $target.attr('data-wrd-id');
    var txt_rest = $target.attr('data-rest-text');



    $('.add').hide();
    $('.update,.cancel,.delete').show();


    var $combobox_word = $('#combobox_word');
    var $txt_rest = $('#txt_rest');

    $combobox_word.val(wrd_id);
    // $('.ui-autocomplete-input').refresh();
    $("#combobox_word").combobox('autocomplete', wrd_id);
    $txt_rest.val(txt_rest);

    //$txt_rest.val();
    $('[in_editing]').removeAttr('in_editing');
    $(element).parent().attr('in_editing', 1);
    $('.search-box').addClass('search-box-edit');
}


function deleteTreeNode (element) {
    if( confirm('هل انت متأكد من عملية الحذف ؟') ){
        
        $('[in_editing]').remove();
        cancelEditingTreeNote();


    }//end confirm
}


function selectTreeNode (event, element) {
	$('a.selected').removeClass('selected')
    $(element).addClass('selected');  

    event.preventDefault();
}

function openInteractiveDialog(event, element){
    $element = $(element);        
    $('[data-interactive]').removeAttr('data-interactive');
    $element.attr('data-interactive',true);
    new treeNode().bindNodes();
    bindInteractiveDialog($element);
    
    dialogInteractiveMinimizeButton();
    $("#dialog-interactive").dialog('open');

    event.preventDefault();
}
function dialogInteractiveMinimizeButton(){

    var $closeButton = $('[aria-describedby="dialog-interactive"] [title="close"]');
    var $minimizeButton = $closeButton.clone();
    
    $minimizeButton
    .addClass('ui-dialog-titlebar-minimize')
    .attr('title','minimize');

    $minimizeButton
    .find('span:first')
    .removeClass('ui-icon-closethick')
    .addClass('ui-icon-minusthick');

    $minimizeButton
    .find('span:eq(1)')
    .text('minimize');

    $minimizeButton.insertAfter( $closeButton );
    $minimizeButton.click(function(){        
        new treeNode().setRestoreNote();
        $('.maximize-box').show();        
        $("#dialog-interactive").dialog('close');
    });


}
function bindInteractiveDialog($element){ 
    $('#dialog-interactive-project-name').text( $('#tree').attr('project-name') );
    $('#dialog-interactive-sentence').text( $element.attr('data-sentence') );
    $('#dialog-interactive-tax').text( $element.attr('data-taxonomy-nice-name') );  
    var notes =  $element.attr('data-notes');
     $('#dialog-interactive-notes').val(''); 
    if(notes){     
        $('#dialog-interactive-notes').val( decodeURI(notes) );
    }

}


function treeNode(){
    var current,prev, next, up, down; 
    var prevIsDisabled, nextIsDisabled, upIsDisabled, downIsDisabled;

    function clearAttr(){
        $('[dialog-interactive-prev]').removeAttr('dialog-interactive-prev');
        $('[dialog-interactive-next]').removeAttr('dialog-interactive-next');
        $('[dialog-interactive-up]').removeAttr('dialog-interactive-up');
        $('[dialog-interactive-down]').removeAttr('dialog-interactive-down');
        $('[data-interactive-current]').removeAttr('data-interactive-current');
        $('.selected').removeClass('selected');
    }

    this.bindNodes = function(){        
        clearAttr();
        current = $('[data-interactive]');  
        current.attr('data-interactive-current',1);
       
        prev = current.closest('li').prev().find('a:first');
        prevIsDisabled = (prev.size() < 1);
        $('#dialog-interactive-prev').prop('disabled', prevIsDisabled);
        prev.attr('dialog-interactive-prev', 1);

        next = current.closest('li').next().find('a:first');  
        nextIsDisabled = (next.size() < 1);
        $('#dialog-interactive-next').prop('disabled', nextIsDisabled);
        next.attr('dialog-interactive-next', 1);

        up = current.closest('ul').closest('li').find('a:first'); 
        upIsDisabled = (up.size() < 1);
        $('#dialog-interactive-up').prop('disabled', upIsDisabled);
        up.attr('dialog-interactive-up',1);

        down = current.parent().find('li:first').find('a:first');         
        downIsDisabled = (down.size() < 1);
        $('#dialog-interactive-down').prop('disabled', downIsDisabled);
        down.attr('dialog-interactive-down',1);
        
        if( !current.attr('data-image-src') ){
            var default_src = $('#dialog-interactive-image-upload').attr('data-def-src');
            $('#dialog-interactive-image-upload').attr('src', default_src);
        }else{
            var src = current.attr('data-image-src');
            $('#dialog-interactive-image-upload').attr('src', src);
        }
    }
    this.bindNodes();

    function _setCurrent(element){
        $('[data-interactive]').removeAttr('data-interactive');
        // $('[data-interactive-current]').removeAttr('data-interactive-current');

        element.attr('data-interactive', true)
               // .attr('data-interactive-current', true)

    }
    this.setRestoreNote = function(){
        $('[data-interactive-current]').removeAttr('data-interactive-current');
        current.attr('data-interactive-current', true)
               
    }

    this.movePrev = function(){     
        if(!prevIsDisabled){
            _setCurrent(prev);
            this.bindNodes();
            return current;
        }
        return false;
    }

    this.moveNext = function(){
        if(!nextIsDisabled){
            _setCurrent(next);
            
            this.bindNodes()
            return current;
        }
        return false;
    }

    this.moveUp = function(){        
        if(!upIsDisabled){
            _setCurrent(up);
            
            this.bindNodes()
            return current;
        }
        return false;
    }

    this.moveDown = function(){        
        if(!downIsDisabled){
            _setCurrent(down);
            this.bindNodes()
            return current;
        }
        return false;
    }


    this.move = function(direction){

        if(direction == 'prev'){
            return this.movePrev();
        }

        if(direction == 'next'){
            return this.moveNext();
        }

        if(direction == 'up'){
            return this.moveUp();
        }

        if(direction == 'down'){
            return this.moveDown();
        }
    }
}


function moveInteractiveNode(direction){
    $element = new treeNode().move(direction);
    if($element !== false){
        bindInteractiveDialog($element);
    }
}