$(document).ready(function() {

    $("#dialog-interactive").dialog({
        autoOpen: false,
        resizable: true,
        height: 550,
        width: 800,
        modal: true
    });// dialog-confirm


});// document.ready


function dialogInteractiveNotesChange(event, element){
    $('[data-interactive]').attr('data-notes', encodeURI( element.value ) );
}