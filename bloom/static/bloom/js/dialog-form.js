$(document).ready(function() {
    $("#dialog-form").dialog({
        autoOpen: false,
        height: 300,
        width: 350,
        modal: true,
        buttons: {
            "تعديل": function() {
                var wrd_id = $('#dialog-form input:eq(0)').attr('wrdid');
                var verb = $('#dialog-form input:eq(0)').val().trim();
                var orignal_wrd = $('#dialog-form input:eq(0)').attr('orignal_wrd');

                $.post("/bloom/verb_update/", {
                    csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    'verb': verb,
                    'wrd_id': wrd_id
                }).done(function(data) {
                    $(".top-words[wrdid='" + wrd_id + "']").text(verb)
                    $('#tree a.tree_node').each(function() {
                        var wrd = $(this).text().split('الطالب')[0].substring(3).trim();
                        var rest = $(this).text().split('الطالب')[1].trim();
                        //alert('wrd :' + wrd + '\n rest :' + rest);
                        if (wrd == orignal_wrd)
                            $(this).text(' أن ' + verb + ' الطالب ' + rest);



                        $("[id^='tabs\-']").each(function() {
                            $this_tab = $(this);
                            var wrds = [];
                            var i = 0;
                            $("[id^='wrd_']", this).each(function() {
                                wrds[i++] = $(this).text();
                            });
                            $this_tab.attr('wrds', wrds);
                            $("[id^='txt_search']", $this_tab).autocomplete({
                                'source': wrds
                            });

                        });

                        $("#dialog-form").dialog("close");
                    });

                });


            },
            'الغاء': function() {
                $(this).dialog("close");
            }
        }

    });//end dialog-form 

 
    $(document.body).on('click', '.new_wrd_edit', function () {
        $("[id^='btn_cancel']").trigger('click');
        next_item = $(this).next();
        $('#dialog-form input:eq(0)').val(next_item.text()).
            attr({
                'wrdid': next_item.attr('wrdid'),
                'orignal_wrd': next_item.text()
        });

        $("#dialog-form").dialog("open");

        return false;
    });
        


});//end docuemt.ready
