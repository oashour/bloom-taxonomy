$(document).ready(function() {

    $("#dialog-confirm").dialog({
        autoOpen: false,
        resizable: true,
        height: 300,
        width: 500,
        modal: true,
        buttons: {
            "اضافة الفعل": function() {
                var taxonomy_id = $('#dialog-confirm-taxonomy').val();
                var verb = $('#dialog-confirm-verb').val();


                $.post("/bloom/verb_add/", {
                    csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    'verb': verb,
                    'taxonomy_id': taxonomy_id
                }).done(function(data) {
                    var $combobox_word = $('#combobox_word');

                    $('<option>', {
                        text: verb,
                        value: data.id,
                        'data-taxonomy': data.taxonomy,
                        'data-taxonomyid': taxonomy_id
                    }).
                    appendTo( $combobox_word )


                    


                    $combobox_word.val(data.id);
                    // $('.ui-autocomplete-input').refresh();
                    $("#combobox_word").combobox('autocomplete', data.id);

                });



                $(this).dialog("close");
            },
            'الغاء الامر': function() {
                $('.custom-combobox-input').val('');
                $(this).dialog("close");
            }
        }

    });// dialog-confirm


});// document.ready
