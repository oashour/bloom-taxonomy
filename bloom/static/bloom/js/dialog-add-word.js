$(document).ready(function() {

    $("#dialog-add").dialog({
        autoOpen: false,
        resizable: true,
        height: 200,
        width: 450,
        modal: true,
        buttons: {
            "حفظ": function() {
                var taxonomy_id = $('#dialog-confirm #verb').attr('taxonomy_id');
                var verb = $('#dialog-confirm #verb').text().trim();


                $.post("/bloom/verb_add/", {
                    csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                    'verb': verb,
                    'taxonomy_id': taxonomy_id
                }).done(function(data) {
                    var wrd_id = data;
                    var element = $("[taxonomy_id='" + taxonomy_id + "'] > ul:eq(0)").find('a:eq(0)').clone();
                    var element_href = element.attr('href');

                    $(element).attr({
                        'id': 'wrd_' + wrd_id,
                        'wrdid': wrd_id,
                        'href': element_href.slice(0, element_href.lastIndexOf('\-')) + 'wrd' + wrd_id
                    }).text(verb).addClass('new_wrd');

                    $("[taxonomy_id^='" + taxonomy_id + "'] > ul:eq(0)").append($('<li></li>').append(element));

                    wrds = $("[taxonomy_id^='" + taxonomy_id + "']").attr('wrds') + ',' + verb;
                    $("[taxonomy_id^='" + taxonomy_id + "']").attr('wrds', wrds);

                    $("[taxonomy_id^='" + taxonomy_id + "']").
                    find("[id^='btn_add_']").
                    trigger('click');

                    $("[taxonomy_id^='" + taxonomy_id + "']").
                    find("[id^='txt_search']").
                    autocomplete({
                        'source': wrds.split(',')
                    });


                    $(element).before('<a href="#" class="new_wrd_edit"></a>');

                    //$("[taxonomy_id^='" + taxonomy_id + "'] p:eq(0)").                            
                    //    append(item)


                });



                $(this).dialog("close");
            },
            'لا': function() {
                $(this).dialog("close");
            }
        }

    });// dialog-confirm


});// document.ready
